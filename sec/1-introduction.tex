\chapter{Introduction} \label{ch:1}

\epigraph{\textit{In 1971 the fastest car in the world was the Ferrari Daytona, capable of 280kph. The world’s tallest buildings were New York’s twin towers, at 415 meters. In November that year Intel launched the first commercial microprocessor chip, the 4004, containing 2.300 transistors, each the size of a red blood cell. [...] Since then chips have improved in line with the prediction of Gordon Moore, Intel’s co-founder: processing power doubles roughly every two years as smaller transistors are packed ever more tightly onto silicon wafers, boosting performance and reducing costs. [...] If cars and skyscrapers had improved at such rates since 1971, the fastest car would now be capable of a tenth of the speed of light; the tallest building would reach half way to the Moon.}}{\textit{The future of computing}\\  The Economist --- Mar. 12th 2016 issue}

\section{Motivation}

Parallelism is a very important topic in computer science and industry today. For almost 20 years the performance of microprocessors increased, on average 50\% per year relying in something called \textbf{frequency scaling}: as transistors evolved, they became smaller and faster, meaning that users and developers could just wait for the next generation to improve the speed of their applications. Since 2002, this average has slowed to about 20\% because some physical limits are being met, resulting in a major change in microprocessor design: by 2005, the major manufacturers realized that the way to increase performance from then on laid in the direction of parallelism and started putting multiple complete processors on a single integrated circuit. \par

\begin{figure}[h!]
	\includegraphics[width=\textwidth]{fig/timeline.pdf}
	\caption{Parallel computing timeline.\cite{FresnoBausela2015} \label{fig:timeline}}
\end{figure}

But not everything started with the end of frequency scaling. Parallelism has come a long way since the 1960s, when the first supercomputers were
introduced. Therefore, it has been usually associated with high performance computing. Also, the evolution of technology has made possible hardware upgrades that got better each passing year. This change has been recorded by the Top500 project\cite{top500}, which ranks the 500 most powerful computers in the world: since the the project started in 1993, the performance of the top system in the list has always shown a steadily grown and current supercomputers are massive parallel machines with millions of processing units. Some important events for parallelism can be seen in fig.\ref{fig:timeline}.

This project aims to be a practical approach to modern parallel programming frameworks and prove its value by implementing and improving the performance of a consumer algorithm such as a video-stabilization method.

The document is structured as it follows: this first chapter reviews the main parallel models and gives details on some popular implementations of them in use nowadays. Chapter \ref{ch:2} gives some insight into the theory that supports the parallelism itself and present some broad terms to help the reader understand the rest of the document. In Chapter \ref{ch:3} we describe the GC-BPM, the video stabilization algorithm selected to enlighten the frameworks comparison as well as the computer vision work environment required to start working on its implementation, which is later described in detail in Chapter \ref{ch:4} along with its equivalent in the different tools selected: OpenMP, CUDA and OpenCL. Chapter \ref{ch:5} would present the experiments that have to be carried out to test the improvement achieved by executing the algorithm in multiple processing elements and how this can be translated to numbers. And to conclude, the last chapter would summarize all the work made by the author in this Thesis and describe some feasible lines for future work.

\section{Parallel Programming Models and APIs}

Models exist as an abstraction above hardware and memory architectures to simplify its comprehension and offer a version of the underlying system, in this sense, we have to talk about programming models to refer the style of programming where execution is invoked by making what appears to be encapsulated library calls. What distinguishes a programming model from a normal library is that the behavior of the call cannot be understood in terms of the language the program is written in.

In parallel computing, the execution model usually exposes features of the hardware to achieve high performance. The large amount of variation in hardware causes a need for numerous parallel execution models and because it is not practical to make a new language for each it is common practice to invoke parallel execution model behaviors, like the creation of a thread or the launch of a kernel, via an API.

\subsection{Shared Memory}

This is the most rudimentary form of parallelism, and it's often used in combination with some others. Processes share a common address space, which they read and write asynchronously using mechanisms such as locks and semaphores to prevent data races and deadlocks.

Shared memory machines, OS, compilers and hardware support this form of programming. Some examples are:

\begin{itemize}
	\item POSIX standard shared memory APIs.
	\item UNIX shared memory segments (\texttt{shmget}, \texttt{shmat}, \texttt{shmctl}, ...).
\end{itemize}

\subsection{Threads}

They are an extension of the shared memory techniques, introduced by E.W. Dijkstra in his paper \textit{Cooperating sequential processes} where he proposed a model for concurrent operating systems based on ``heavy'' process with multiple lightweight concurrent execution paths, which after were renown as threads. Since then, many implementations have been created to exploit the multi-threading model:

\begin{itemize}
	\item \textbf{Pthreads} (POSIX Threads) - consist in both an execution model and API, and are available as standard in most mainstream OS. It offers a low level interface to control everything regarding threads: from their management to synchronization, etc.
	\item \textbf{OpenMP} - High-level extension for C/C++. It allows many programs to be parallelized with relative ease and a few compiler directives. It appeared in 1997 as the joint effort of a consortium of manufacturers to simplify the parallelization of some constructions such as static loops (where the number of iterations is fixed) like the ones that can be found in numerical computations with matrices.
\end{itemize}

\subsection{Message Passing}

This model is oriented and best suited for distributed memory machines but it can be translated to any kind of machine as it is an abstraction for complex systems, at expense of the communication and synchronization steps falling into the hands of the programmer. Implementations are usually presented as a library of functions, directly called from the source code.

\subsubsection{MPI - Message Passing Interface}

Message passing libraries have been around since the 1980s, when their relevance increased to the point of creating a forum to standardize a implementation, resulting on the MPI specification, ``de facto'' standard for message passing nowadays. 

Using MPI has many advantages. It can be used to solve a wider range of problems than thread libraries and, as said before, the programs can run on either shared- or distributed-memory architectures. On the other hand, creating a parallel application with MPI can be complex, because the programmer has to determine the data dependencies, divide the algorithm into tasks, and then implement the message passing and synchronization.

\subsection{General-Purpose Computing on GPUs} \label{subs:gpgpu}

The GPUs (Graphics Processing Unit) were designed initially just for graphics and rendering images by computer, but as manufacturing became cheaper and they proved to be deeply programmable and accurate with a very low costs (power consumption compared to regular purpose CPUs is only a fraction) they started to make appearance in supercomputers while beating in some aspects the performance of traditional CPUs as can be seen in figure \ref{CPUvsGPU}.

Since NVIDIA launched its model Tesla GPU card in 2007, the use of such coprocessors along with the general-purpose processors has become a big trend. Many of the latest supercomputers are based on accelerators that have specialized hardware making them suitable for particular types of computation. Classified as hybrid manycore systems, they include lots specific-purpose processors packed with their own instruction set and multilevel hierarchy of memory.

\begin{figure}[h!]
	\includegraphics[width=\textwidth]{fig/gflops-sp.pdf}
	\caption{Comparison of annual theoretical peak GFLOP/sec in single precision. \\ Higher is better.\cite{karlRupp} \label{CPUvsGPU}}
\end{figure}

In the framework of accelerating computational codes by parallel computing on graphics processing units (GPUs), the data to be processed must be transferred from system memory to the graphics card's memory, and the results retrieved from the graphics memory into system memory. In a computational code accelerated by general-purpose GPUs (GPGPUs), such transactions can occur many times and may affect the overall performance, so that the problem of carrying out those transfers in the fastest way arises. We can sum up what happens in a GPUs as it follows:

\begin{enumerate}
	\item Setup inputs on the host (CPU memory)
	\item Memory allocations:
	\begin{enumerate}
		\item Inputs on the GPU
		\item Outputs on the host
		\item Outputs on the GPU
	\end{enumerate}
	\item Copy inputs from host to GPU
	\item Start GPU kernel
	\item Transfer the output from GPU to the host
\end{enumerate}

\subsubsection{CUDA}

The CUDA platform is just a software layer that gives the programmer direct access to the GPU's virtual instruction set and parallel computational elements. To put it in plain words, CUDA extends the GPU functionality to general purpose computing. It abstracts the parallelism details relying in the concept of \textbf{kernels}, which are just plain C functions that, when called, are executed in parallel by N different CUDA threads. \cite{nvidia}

\subsection{Hybrid}

They combine more than one of the previously described models to suit the combination of architectures they run in and they are currently the most popular model exploited on clusters of multicore machines. Some examples in use we can mention are made combining some of the already described frameworks such as \textbf{OpenMP + MPI} or \textbf{MPI + GPGPUs}. It requires to mix several tools with different programming models. Moreover, the appearance of accelerators has added an additional layer of complexity: A programmer must be proficient in MPI, OpenMP, and CUDA (or equivalent) to be able to take advantage of the current generation of parallel systems.\cite{FresnoBausela2015}

\subsubsection{OpenCL}

As happened with OpenMP, when hybrid parallelism mixing CPUs and GPUs started to gain popularity, a consortium was formed by the main hardware and software manufacturers to develop a framework for supporting heterogeneous computing.

The main advantage of using OpenCL is that is the first open, royalty-free standard for \textbf{cross-platform} processors and GPUs allowing high level of portability while keeping the abstraction layer relatively close to the hardware, minimizing the overhead. As CUDA, it uses kernels as execution model, which in this case, can speak with many different devices. But all this advantages don't come without a price, and this one is that is not an easy framework to learn \cite{intro_openCL}: it comprises some unique data structures and functions and a particular way of doing as we will discuss later.