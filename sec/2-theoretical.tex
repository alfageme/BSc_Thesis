\chapter{Theoretical Foundations} \label{ch:2}

\section{General Aspects of Parallelism}

When someone explains what is an algorithm to a broad public, usually starts by making a real-world analogy with a food recipe: a computation is just a problem broken into a discrete series of instructions, which traditionally were executed \textbf{sequentially}, that is in the order they were written, one by one by a single processor. This could fit pretty much as the definition for traditional serial computing.

\subsection{Concurrent versus Parallel}

At its simplest, parallel computing is the \textbf{simultaneous} use of multiple compute resources to solve a problem. When stepping into parallelism for the first time, one can be confused with concurrency concepts because the illusion of multiple tasks running at once. But a difference should be pointed: concurrent programming is just a form of computing in which several computations are executing during \textbf{overlapping} time periods, while parallel has some special characteristics:

\begin{itemize}
	\item More than one \textbf{simultaneous processes} are running at the same time physically.
	\item There's a need for communication and/or synchronization systems to put together the results of these processes.
\end{itemize}

\subsection{What Can Be Achieved With Parallelism?}

Many computing problems are so huge that they cannot be solved sequentially in a reasonable time. In the natural world, many complex, interrelated events happen at the same time, yet within a temporal sequence. Compared to serial computing, parallel computing is much better suited for \textbf{modeling, simulating and understanding complex, real world phenomena} \cite{intro_parallel}. And something to take into account is that computer modeling is a never-ending run so every advance in performance just makes the models more detailed.

\subsubsection{How is the improvement of the parallelism measured} \label{sec:2.1.2}

Generally, we measure the performance of computer programs based on how much time do they need to run, this usually turns out to be directly related to the size of the input data set the program works with. If we translate this to the parallel world, and as squeezing more cores in an integrated circuit or rewriting an application to take advantage of parallelism is not cheap, we have some results to take into account before considering these options.

\begin{itemize}
	\item \textbf{Speedup}: is used to measure the parallelism efficiency. For a given data set of size $n$, it is described as the ratio:

	\begin{equation} \label{eq:speedup}
        S=\frac{t(n)}{t(n,p)}
	\end{equation}

	\begin{itemize}
		\item $t(n)$ the reference sequential algorithm run time.
		\item $t(n,p)$ the parallel run time when using $p$ processors.
	\end{itemize}

	\item \textbf{Scalability}: is the property to keep the speedup growing constantly as the number of processors increase.
\end{itemize}

\subsubsection{What cannot be achieved with parallelism: Amdahl's law}

To some inexperienced eye, parallelism could look like the holly grail of computing: just add more processors and divide the time it takes to programs to run. Far from reality; Gene Amdahl noted\cite{amdahl1967validity} that the potential program speedup is defined by the fraction of code ($t_{p}$) that can be parallelized, which could not be as high as 100\% as communications, synchronization or I/O must be carried out in a sequential fashion. We can realize from these notes that there are some limits to the scalability of parallelism. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\textwidth]{fig/AmdahlsLaw.pdf}
    \caption{Speedup as processing units increase for different portions of parallelizable code. Source: \url{https://commons.wikimedia.org/wiki/File:AmdahlsLaw.svg}}
    \label{fig:amdahlslaw}
\end{figure}

If we decompose the run times in both members of the speedup fraction \ref{eq:speedup} based on how much of the program cannot be parallelized ($t_{s}$) we have the following equation.

\begin{equation}
	\lim_{p \to \infty} S = \lim_{p \to \infty} \frac{t_{s}+t_{p}}{t_{s}+\frac{t_{p}}{p}} = 1 + \frac{t_{p}}{t_{s}}
\end{equation}

As the number of processing elements tend to be infinite, the speedup reaches its asymptotic limit as can be seen in graphic \ref{fig:amdahlslaw}. Meaning that, after a finite number of processors adding more just results in the same speedup, which can be translated in same run time for the same problem size.

In 1988 J.L. Gustafson \cite{gustafson1988reevaluating} made some notes about Amdahl's law limitations, because it was only based in a fixed workload size, the sequential fraction of the program does not change while increasing the resources. Instead, Gustafson proposed to set the size of problems to fully exploit the computing power that becomes available as the resources improve. Under this vision, as equipment becomes faster, larger problems can be solved within the same time.

\subsection{Parallel Architectures: Flynn's Taxonomy}

In order to organize parallel machines, we can rely on Flynn's taxonomy\cite{flynn1972some}, it distinguishes multi-processor computer architectures according on to how they can be classified along the two independent dimensions of \textbf{Instruction} and \textbf{Data} stream (sequences).

\begin{table}[h!]
	\centering
	\begin{tabular}{|c|c|c|}
	\hline
				  & Single Instruction & Multiple Instruction \\ \hline
	Single Data   & SISD               & MISD                 \\ \hline
	Multiple Data & SIMD               & MIMD                 \\ \hline
	\end{tabular}
	\caption{Matrix defining the 4 architectures in Flynn's taxonomy}
\end{table}

\begin{itemize}
	\item \textbf{SISD}: Represents traditional serial (non-parallel) computer.
	\item \textbf{SIMD}: Single instruction, meaning processing units execute the same instruction at any given clock cycle, but each one can operate on a different data element (because of this, it's well suited for \textit{specialized problems} with a high degree of regularity, such as graphics processing). Two subgroups of this category can be distinguished:
		\begin{itemize}
			\item \textbf{Processor Arrays}: e.g. Streaming Multiprocessors (SM) located in some modern GPUs.
			\item \textbf{Vector Processors}: like the very first parallel computers, which introduced vector registers to allow operating with several data at once, still present in most of today's commercial CPUs. 
		\end{itemize}
	\item \textbf{MISD}: Separate instruction streams in contrast to a single data stream that feeds the processing units. Few examples ever existed, but some conceivable uses could be those requiring redundancy or multiple filters feed by the same data.
	\item \textbf{MIMD}: The most complex and common form of parallelism: every processor may be executing a different instruction stream with a different data stream than the others, so execution can be synchronous or asynchronous, deterministic or non-deterministic. Many MIMD architectures also include SIMD. Too broad to be useful on its own; is typically decomposed according to memory organization. We will review that in next section.
\end{itemize}

\subsection{How Do We Write Parallel Programs}

As it's neither trivial nor easy to write translators that convert sequential programs to parallel, we do need to actually rewrite our programs using a parallel approach and to do so we depend on the basic idea of partitioning and distributing the work to be done among the processing elements. When these can work independently, writing a parallel program could be much the same as the serial version, but problems appear when the data and the results are dependent on each other, like finding a matrix minimal or sorting a list of numbers.

There are two broad, but not closed, classes used when talking about parallel algorithm design:

\begin{itemize}
	\item \textbf{Data parallelism} -- where parallelism translates into performing the \textbf{same operation} to different data at the same time.
	\item \textbf{Task parallelism} -- in which parallelism is the result of different computations at the same time. A fundamental form of task parallelism is pipelining, the technique exploited in the already mentioned vector processors.
\end{itemize}

As rule of thumb, we can say PCAM (also known as Foster's) Methodology \cite{foster1995designing} proposed by Ian Foster is a comprehensive summary on which steps should be taken to transform a sequential algorithm in its parallel variant. It comprises 4 stages, which initials give the methodology its name. 

\begin{itemize}
	\item \textbf{P}artition: split both the computation and the data into a large number of small tasks.
	\item \textbf{C}ommunication: identify the necessary communication between the tasks to perform the computation. % important!
	\item \textbf{A}gglomeration: required to achieve data locality and good performance as fine-grained partitioning is usually not an efficient parallel design. They can be combined into tasks of larger size.
	\item \textbf{M}apping: the final step is just to distribute the tasks between the processors available, following some guidelines:
    \begin{itemize}
        \item Tasks that can execute concurrently map to different processors.
        \item Tasks that communicate frequently map to the same processor.
    \end{itemize}
\end{itemize}

\section{Parallel Memory Architectures}

Computer memory architectures describe the way data is stored for the processor to access. As this is a fairly complex problem, again, we usually employ abstractions or models to simplify its understanding and isolate unnecessary details. Since the early era of computing, one model has remained as virtually all computers have followed, it is von Neumann architecture or ``stored program computer''. It comprises four main components: Input/Output, memory, a control unit and an arithmetic-logic unit. Both the program instructions and the data are stored in the memory, fetched by the control unit and then executed. This model is relevant for our purpose as parallel computers still follow this design, just multiplied in units.

As mentioned before, MIMD machines are too broad for most of the approaches as they comprise the majority of parallel implementations. There is a mainstream basic classification of them according to 2 criteria:

\begin{itemize}
	\item How the data in memory is accessed.
	\item What kind of synchronization and communication mechanisms they use.
\end{itemize}

\subsection{Shared Memory}

Generally, these machines have in common the ability for all processors to access all the available memory as a global address space. According on how the memory access and the processors are, they're classified in:

\begin{itemize}
	\item \textbf{Uniform} Memory Access (UMA) - represented by Symmetric Multiprocessor machines (SMPs, like the ones found in graphic cards), with many identical processors (SMs), resulting in equal access and access time to shared memory and cache coherence at hardware level, because of this there is no need to distribute data among processors. These do not scale well so they are limited to a small number of processors.
	\item \textbf{Non-Uniform} Memory Access (NUMA) - made by linking 2 o more SMPs, slowing down memory access across the links. To mitigate this effect, each processor has a cache storing the memory contents of some others, along with a protocol to keep the cache contents coherent.
\end{itemize}

\subsection{Distributed Memory}

As opposed as what happened in shared memory architectures, in these machines each processor has its own local memory and there's no such thing as global address space and to share the data among processors, they require a communication network. So when a processor needs access to data in another, it is the programmer responsibility to define the communications. Usually the inter-processor communication is achieved by message-passing. Also, as the compute nodes are not strictly connected, the need to synchronize appears and also becomes responsibility of the programmer.

Based on topology and technology used for interconnection, communication speed can range from almost as fast as shared memory to orders of magnitude slower. Two computer architectures fall in this category:

\begin{itemize}
	\item Clusters (in Linux environments called Beowulf) which are the most popular and common form of distributed systems for parallelism. A cluster comprises a collection of independent nodes collaborating to achieve a common goal.
	\item Massive Parallel Processors (MPP) where processor and network infrastructure are tightly coupled physically.
\end{itemize}

\subsection{Hybrid and Heterogeneous architectures}

These machines use both shared and distributed memory architectures to increase the scalability at program complexity cost.

\section{Benchmarking Techniques} \label{sec:benchmarking}

As mentioned in section \ref{sec:2.1.2} a very important aspect when working developing parallel algorithms is to measure the time they need to execute and the speedup achieved by these techniques, first of all, to know if we are doing it right, and second, to explore the limits of what can be done with it.

Based on the specifics of the framework chosen, there are different approaches to measure the run time as they imply different models and behaviors. Here are some examples that have been taken from Gerassimos Barlas's book \cite{barlas2014multicore}. 

\subsubsection{OpenMP}

This case is pretty straightforward, as in every program using this API, there is an explicit point where the program launches the different threads, this marks when we can start measuring the time taken for the parallel fraction of the program. This can be achieved using some functions like \texttt{omp\_get\_wtime()}. \\

\lstinputlisting[style=custom, numbers=none]{code/openMP.c}

\subsubsection{Kernel-based models: CUDA and OpenCL}

As there's many more details to take into account, these differ from the thread model, but as the platforms have evolved, the framework have as well, including some workarounds in the form of primitives and global variables. As example, in CUDA environments, the host can measure the timing of events (like kernel launches, memory transfers, etc.) on the GPU by inserting \texttt{cudaEvent\_t} object in the streams.

\lstinputlisting[style=custom, numbers=none]{code/events.cu}

\subsubsection{Simplified Approach} \label{subs:time_simplified}

After developing some of the different solutions, we realized that including the previous techniques was increasing hugely the complexity of the whole code, since they are more suited for a single-framework program rather than our three legged implementation. The final function used in the code was proposed and developed by Javier Fresno, a member of Trasgo Group of the University of Valladolid \cite{trasgo}.

\lstinputlisting[style=custom, numbers=none]{code/cp_Wtime.c}