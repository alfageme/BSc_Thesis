\chapter{Algorithm} \label{ch:3} 

\section{Overview}

As video recording is mostly an error-prone process as it usually relies in the manipulation of some device by a human operator, the need to develop video post processing systems to avoid this kind of problems makes this an interesting field of study. Historically, many methods for image stabilization have been considered \cite{stabilizationMethods} and evolved hand in hand with hardware.

\begin{itemize}
    \item Naive approach: One point track --- locks the focus on a bright object in the background and keeps it in the exact same place, no matter how much the camera moves.
    \item Multiple point track: as we include more tracking points into account, we make possible to correct rotation or scaling in the original image, considering it only behaves well with static (meaning without motion) shots.
    \item Block Matching Algorithms: that use a post-process approach, where multiple video frames are used to determine the direction of the movement of the whole sequence.
    \item Advanced image post-processing: relies in filters that take into account many data and transformations. Some examples are the Deshaker algorithm included in VirtualDub or the Warp Stabilizer that comes with Adobe After Effects.
    \item Approaches based on metadata captured by extra elements integrated in modern video capture devices like gyroscopes, rolling shutters, etc.
\end{itemize}

To give some practical insight to this project we're going to focus in creating a parallel implementation of the \textbf{Gray-Coded Bit-Plane Matching} or GC-BPM algorithm proposed in\cite{GC-BPM1999}. This solution (which, as its name suggests, could fall in the Block Matching category) is an approach fairly straightforward with quite good results for non-rotating video sequences.

The main goal of this stabilization procedure is to calculate the ``global'' motion vector (GMV) of each video frame relative to the previous one. This vector tries to represent roughly the hand movement of the camera operator. With this information, we can determine whether the video sequence is displacing naturally or shaking unwittingly, and use it in a post process stage to generate a stabilized video sequence.

We can start by transforming each frame of the video into a grayscale image in which each pixel with coordinates $(x,y)$ will be coded with 8 bits ($K=8$) as it follows:

\begin{equation}
    f^{t}(x,y) = a_{K-1}2^{K-1}+a_{K-2}2^{K-2}+\ldots + a_{1}2^{1}+a_{0}2^{0}
\end{equation}

After obtaining these 8 bit planes, we can use them to compute their gray-code equivalent. The gray-coding step is quite relevant for this algorithm because it allows the next block to estimate the motion, even using a single bit-plane, by encoding most of the useful image information into a few of the planes. With the gray code, small changes in gray level yield a small, uniform change in the binary digits representing the intensity, meaning motion between frames. We could calculate this planes as:

\begin{align}
    \label{eq:gray-code}
    g_{K-1} & = a_{K-1} \\
    g_{k}     & = a_{k} \oplus a_{k+1}, \; 0\leq k \leq K-2
\end{align} 

At each coordinate $(m,n)$ within the search subimage, the matching method calculates $C_{j}$: a correlation matrix that represents the number of displaced pixels from the previous frame $g^{t-1}_{k}$, as:

\begin{equation}
    C_{j}(m,n) = \frac{1}{MN} \sum^{M-1}_{x=0}\sum^{N-1}_{y=0} g^{t}_{k}(x,y) \oplus g^{t-1}_{k}(x+m,y+n), \;  -p \leq m,n \leq p
\end{equation}

From there, we can obtain the 4 different local motion vectors $V_{j}$ as the one pointing to the coordinate where this correlation is minimum for each subimage:

\begin{figure}[h!] \label{fig:BPM}
	\includegraphics[width=\textwidth]{fig/BPM.pdf}
	\caption{Estimation of local motion vectors from four subimages in a Gray-coded bit-plane}
\end{figure}

\begin{equation}
    V_{j} = arg \, \min{\{C_{j}(m,n), -p\leq m,n \leq p \}}  \;  \forall j = \{1..4\}
\end{equation}

At this point, we could carry on with the proposed on the paper \cite{GC-BPM1999} 3 Steps Search (3SS) to determine the motion vector using different bit-planes refining the search and reducing the computational complexity, but this doesn't contribute at the main purpose of our work, so we will use just the 6th bitplane as one of the tree mentioned with relevant information for our work. This been said, to estimate our final result we only need to apply a median filter; this is, to calculate the median for each component of the vectors involved, just to select the most representing vector as global. For this operation we would to take into account $V_{g}^{t-1}$, which is the global motion vector obtained by the algorithm in the immediately preceding frame, to feedback the filter:

\begin{equation}
    V_{g} = \text{median}\{V_{1}^{t},V_{2}^{t},V_{3}^{t},V_{4}^{t},V_{g}^{t-1}\}
\end{equation}

\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{fig/GMV_obtention.pdf}
    \caption{Obtaining the global motion vector from the previous one and the 4 local vectors for each frame}
    \label{fig:GMV_obtention}
\end{figure}

After gathering this information for all the single video frames, we could start the post-processing phase and use each GMV to precisely crop and center the original colored image sequence to generate a complete and fairly stabilized video, losing some resolution in the way, as described in the appendix \ref{app:results}.

\section{Work enviroment}

To port all this math formulae and data transformations into code legible to a computer, two tasks appear to take into account:

First of all, in order to make the whole process ``visible'', the need to construct some kind of wrapper to \textbf{manage the different video transformation steps} arises: from the input sequence to the stabilized result. And also to be a bridge between the inputs, the different implementations of the algorithms and the outputs. As this is not a project about computer vision, we tried to avoid as much detail in this aspect as possible. We used OpenCV's C++ library for this purpose as it is a very mature framework with a quite soft learning curve, while providing some usable high level functions and data structures to work with computer vision that abstract some of the functionality to be carried out by this wrapper.

\begin{enumerate}
    \item Video to single frames conversion, to load all the video frames in memory to make them manageable. They will be represented as OpenCV's \texttt{cv::Mat} data structures and loaded into a \texttt{std::vector} to represent the video. There's an important fact to take into account at this point: OpenCV has a well known bug (\url{http://code.opencv.org/issues/1287}) that causes the frame count to be incorrect when the video input has been modified with some tool without altering its metadata, this causes the program to fail at initial stages. The possibility to replace OpenCV in the video loading process keeps open to future work.
    \item Transform single frames into bit planes: first of all losing all the color information to turn into grayscale images, decompose them into the different bit planes and use these to compute the gray-code planes as exposed in the equation \ref{eq:gray-code}. Then select the one that comes with relevant information (those identified as so by the paper \cite{GC-BPM1999} were the 4th, 5th and 6th), in our case it will be the 6th one. Finally store them in the original vector to feed the algorithm.
\end{enumerate}

\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{fig/scheme.pdf}
    \caption{Different preprocessing steps made by the video wrapper.}
    \label{fig:scheme}
\end{figure}

The second problem to address is the translation of the GC-BPM procedure into code capable of being parallelized. We could take advantage of previous work implementing this algorithm in C/C++ \cite{Drahansky2009} and Matlab \cite{Brooks2003} as reference for our work. The different algorithm implementation would be encapsulated in diverse functions that will take both the current and the previous bit planes as arguments as well as the $x$ and $y$ components of the previous frame's global motion vector and a couple of pointers to the current frame equivalent components that are set inside the function as they are calculated.

The inputs for all the solutions must specify the full path for the video file and, when talking about any of the parallel solutions, the number of processing units to be used to run the algorithm. And as to the outputs of the program concerns, every implementation must return two different pieces of information: the sequence of global motion vectors ordered according to the frame they belong and in first place and more important for the present case, the time measurement mark for the particular run as determined in-program by the function described in section \ref{subs:time_simplified}.
