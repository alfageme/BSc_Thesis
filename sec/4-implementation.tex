\chapter{Implementation} \label{ch:4} 

\section{Overview} \label{sec:overview}

Before going into specifics, we must discuss the implementation of the video stabilization strategy and some general aspects about the way to take advantage of parallelism in this particular problem. 

We can broadly describe the algorithm in terms of pseudocode: first, we have a loop responsible of calling the stabilization routine passing the two frames required and the previous global motion vector. Then the method itself consisting in 3-nested-loops: one to consider the 4 subimages and 2 more to move the search window over the previous frame's subimage in X and Y directions. The different approaches, as it is usual when working with algorithms that involve operation with matrices goes trough correctly distributing the elements between the available processing elements.

The first way to exploit parallelism in which we can think of, and the most straightforward, is to parallelize the most external loop, the one that just repeats the second point of the process for each one of the subimages. But this will probably won suffice as the workload would be too big for a thread/work-item/processing-unit (stream, to be generic) and only could speed up this fraction of the program by 4, even if there's more processing elements available as happens with the GPUs.

The second approach to the problem involves the displacing search window in both dimensions for each of the 4 subimages that is translated in the two inner loops. This can be, relatively easy \textbf{as it can be reduced to a data-parallel problem}, in which each position of the so called correlation matrix will be filled by a different stream being possible to determine any of the elements of the matrix at any time as all the data is available from iteration one.
 
The problem is not that difficult to aboard in detail: on one hand, we have two shared and read-only memory portions which would be the current frame's central search window and the whole previous frame corresponding subimage. The last will be used inside the different streams to determine the appropriate search window to XOR with the first and carry on. Additionally, other shared memory portion that has to be modified by the streams would be the correlation matrix, each stream only modifies the right cell. The whole matrix contents only would be used when all of the streams have finished filling it, so there is no risk of data races or inconsistences. The only matter then is to distribute the indexes of the matrix between all the processing units. A graphic representation of this idea can be seen in the figure \ref{fig:parallel}.

\begin{figure}[h!]
    \centering
	\includegraphics[width=\textwidth]{fig/parallel.pdf}
	\caption{Parallelization of the 2 inner loops between 4 processors \label{fig:parallel}}
\end{figure}

Obviously, it would be interesting to combine this method together with the one mentioned in the first place as the only thing in which they differ is the subimage used. This could be done either by restructuring the code to include an implicit synchronization operation after all 4 correlation matrices are filled and the coordinates of the minimal calculated to get the GMV. Or, if the language supports it (like in the case of the two kernel-oriented execution models presented in this text), by using a three dimensional index space, where the Z-index would be the one determining the subimage (from 1 to 4).

The last way to board this issue would be to tackle down the data dependence problem between the iterations: as each global motion vector needs to take into account the previous one to be calculated and it might not be available yet. This would translate into parallelizing the (most outer) loop that calls the stabilization method, share the array that contains all the global motion vectors between all threads and synchronize them to calculate the result. This method requires more implicit synchronization mechanisms and would imply more changes in the code to adapt the algorithm because it can be too many operations for a kernel. This solution differs from the ones presented until this point as can be classified as task-parallel instead.

As the reader can guess by the extension of the explanation given or as the method was qualified as ``not that difficult'', we finally choose the second one described for our purpose, it theoretically would offer enough speedup (up to a multiplicative constant of $1/(2 \cdot p)^{2}$ in the parallel fraction of the program if that many processing units are available) which will give enough representative results. 

\section{Solution} \label{sec:solution}

The software was designed in modules to separate all the responsibilities of the program as it involves a certain degree of complexity that must be managed to make it correct and legible at the same time. In this section, we would describe the different modules considered to build the final sequential solution, as it is the cornerstone of the project.

In first place, we needed an unified interface to run the program with all the possible inputs from an standard shell session. The file used for this purpose is \texttt{stabilize.cpp}, it contains the \texttt{main()} method, which handle the command-line arguments validation and also, what we called the ``selector''. It handles the execution of the different solutions based on the first argument passed to the application. As all the functions, sequential or parallel, require the same arguments (the two frames, the previous GMV and the empty pointers to the new), we could define a single entry point for the algorithm and dynamically select the version to be run based on the option selected. 

An object-oriented approach would be to use a \textit{strategy pattern} with a whole class hierarchy, but this will only make the solution more complicated compared, for example with the C-style way, which rely in modifying a function pointer in the switch statement. But as it is less obscure and antique, we could just take advantage of C++11 templates to highly simplify this just by switching between the target functions.

Then and also integrated in the previous file, as it is common to all the solutions, we have the ``wrapper'' we detailed in last chapter that receives and processes the video by dumping all the frames into memory and converting them to gray-code bit-planes by making use of OpenCV functions to ease the way they are later consumed by the algorithm.

A \texttt{commons.h} header file was included to improve the readability of the code and to factor out some common elements for all the versions, e.g. some constants relative to the proportions and internals of the algorithm or the declaration of the functions that handle the mathematical operative implicit to them that are later defined in the \texttt{helper.cpp} source file. Those functions are:

\begin{enumerate}
    \item \texttt{argMin()} -- used to find the coordinates of the minimal value on the squared correlation matrix ($C$) and return them as components of the local motion vector. This procedure should take into account a couple of important matters:

    \begin{itemize}
        \item As the matrix is represented by an array, it is indexed from 0 to $(2 \cdot p - 1)$. But it is originally intended to weight a displacement in a Cartesian plane, so to get the true local motion vector, we should normalize this indexes by subtracting $p$ from the result.
        \item Also by definition of the procedure, in the case of a matrix in which all elements are the same (the result of 2 consecutive search windows containing no displaced pixels from one to another, meaning no movement) It should return the coordinates for the central element $(0,0)$ instead of the first element $(-p,-p)$. 
    \end{itemize}

    \item \texttt{median()} -- this method was extracted from the code provided with \cite{Drahansky2009} (originally called \texttt{median5fast()}) as a fast method to calculate the median of a 5 element vector. It is used twice, for coordinates $x$ and $y$, as the last step of the algorithm to determine the global motion vector.
\end{enumerate}

And last, the file \texttt{stabilization.cpp} contains the ``core'' of the algorithm, this is, where the GMV is determined by taking all the missing steps mentioned in chapter \ref{ch:3}. The code listed here corresponds to the first version developed (\texttt{v1.0}).

\lstinputlisting[style=custom, escapechar=', firstnumber=47, firstline=47, lastline=75]{code/algorithms/sequential_v1.cpp}

But this version had some serious limitations. One of the biggest concerns when working with parallel frameworks, specially in those oriented to GPGPUs, is that the operations we delegate them must be fine-grained enough as the SMs found in GPUs are really simple processors only capable of some basic arithmetic. Also, it is evident that any routine with an entry point in the \texttt{cv} namespace has only been likely compiled for the host CPU and \textbf{will not be usable in device code}, making impossible to call any OpenCV function from a kernel.

However, OpenCV community saw the potential of the parallelism in 2010-2011 and started giving support both for CUDA and OpenCL (\url{http://opencv.org/platforms.html}). This constitutes further evidence that CUDA popularity is huge and increases over time. And also OpenCL's, as we will explain in section \ref{sec:impl_opencl} as the computer vision framework relies part of its standard library in it to accelerate some of the operations when the hardware is available. 

Something that we can point of, as it is directly related to this matter, is that this support offers the equivalent, highly optimized, parallel functions to some of the present in OpenCV's standard library. As an illustrative example, it can be transparent for the developer to parallelize the XOR function in our code to be executed by CUDA simply by calling \texttt{cv::cuda::bitwise\_xor()} (\url{http://goo.gl/RKX68q}).

From these facts, two interesting lines of work were open:

% v1.5

The first one and more straightforward, would be to apply these parallel constructions to our program, making use of \texttt{cv::cuda} and \texttt{cv::gpu} libraries, like the one already mentioned. From an educated point of view, this solution has some pros and cons. It would not exploit the full parallel potential the algorithm has, as it reduces considerably the parallel fraction of the program.

Other disadvantage to study would be the potential overhead that those many calls and memory movements, if not handled properly by the compiler, could have in the overall performance. For example, if memory is not properly coalesced in the device many allocations should be carried in the GPU resulting in considerable delays. But however, it is a very interesting experiment since it relies the algorithm's main operations from already optimized functions to even more optimized parallel kernels.

% v2.0

The other way around the problem would be the natural approach. Since this sort of limitations are determinant to the completion of the project, they lead us to create a second version (\texttt{v2.0}) of the algorithm more generic and framework-independent. The main changes between these two versions can be seen in appendix \ref{app:implementations}. Removing all the data structures and calls made to OpenCV's helper functions from the ``algorithm's core'': the \texttt{cv::Mat} variables and their manipulation, the XOR operation and the count of black pixels in the resulting image. This obviously involves implementing the equivalent helper functions to translate OpenCV data structures to plain-C which will be the ones passed to the threads/kernels to operate and fill the correlation matrix as explained before.

\begin{itemize}
    \item \texttt{splitMat()} -- will be the equivalent of calling OpenCV's \texttt{cv::Rect()} over a \texttt{Mat} object to crop it in 2 dimensions and get a submatrix of it. This is a vital step, since it has to be done for each iteration of the inner loop to determine the search window from a given boundaries and dimensions.
    \item \texttt{mat2arr()} -- is the one that inverts the pixel values and packs all the raw data obtained from a \texttt{cv::Mat} to a generic \texttt{unsigned char} (byte) array. Thanks to OpenCV being mature enough, this translation could be easy if the data in the matrix is contiguous in memory as it is in our case, just by accessing the \texttt{data} member on any \texttt{Mat} object. Also, to take advantage of what we are trying to represent, we could compress every 8-pixels into a single \texttt{unsigned char}, this will improve the memory consumption and the overall performance, as the bitwise operations are carried taking advantage of vector processors, we can XOR 8 pixels at a time. (Or even more, if using even larger data types that also support this behavior).
    \item \texttt{xor2img()} -- the responsible of applying the bitwise XOR operation iteratively to all the elements of both images to get the result.
    \item \texttt{count1s()} -- this simple method calculates the number of bits set to one in an array of bytes, corresponding to the number of black pixels found in the image.
\end{itemize}

These functions had to be tested as they're crucial for the correct operation of the algorithm. So we developed and conducted some sanity tests to check this. Its explanation and results could be seen in the appendix \ref{app:tests}.

\subsection{Problems Found} \label{subs:problems}

While developing and running the different versions of the algorithm, we found a few problems that deserve a special mention because they are closely related to the algorithm's results and performance:

\begin{itemize}
    \item The time required by OpenCV's \texttt{VideoCapture} interface to dump all the frames in memory can be very high for computers equipped with integrated or low-end GPUs, since the work is carried in them. This problem should be addressed by loading the frames on demand, this is, saving just one frame and requesting the next to pass them as the stabilization routine arguments.
    \item While testing the helper functions and comparing results with \texttt{v.1.0}, we found that the way OpenCV's \texttt{bitwise\_xor()} function works is not the same, by definition, than the common bitwise XOR operation, but rather its negation ($\neg(a \otimes b)$) as the figure \ref{fig:cv_xor} shows. This happens because the framework applies the operation at bit-level, but in an grayscale image black pixels are represented by $0s$ instead of $1s$ and the opposite for white ones (\texttt{0xFF}), resulting in the inverse image. This finding made us patch the original version of the algorithm just by adding a \texttt{cv::bitwise\_not()} call after the initial XOR to get the right result.
    \item The different versions of the \texttt{mat2arr()} developed, while correctly packing the data into the byte arrays in linear time ($\mathcal{O}(n/8)$ where $n$ is the number of pixels), were not as efficient in time as using OpenCV's native data structures and operations, since they rely in pointer arithmetic and other optimized techniques that reduce memory movements and allocations.
\end{itemize}

\begin{figure}[h!] 
    \centering
	\includegraphics[width=0.8\textwidth]{fig/xor_prob.pdf}
	\caption{OpenCV's XOR operation \label{fig:cv_xor}}
\end{figure}

The last of the problems lead us to take the decision of limiting the number of frames that are taken into account to be stabilized for the sake of the time needed to run all the experiments. If we had more time, we could further deepen on the reasons behind these operations taking that long and optimize them, but as it is not crucial to the original purpose of the project, we would let it be.

\section{Parallel Implementations}

In the following sections, we will present the specifics of the parallel solutions. Each follows a common structure discussing 3 main topics:

\begin{itemize}
    \item How to compile and build the files required for each solution.
    \item Description and comments on the source code of the solution.
    \item Aspects related to the implementation that will be used in chapter \ref{ch:5} to lend color to the comparative.
\end{itemize}

\subsection{OpenMP} \label{sec:impl_openmp}

Thanks to OpenMP being the result of the effort from a consortium and also from being around the parallel scene since 1997, the libraries required to compile programs that use this API are built-in in most mainstream, contemporary compilers. The only thing to take into account when compiling OpenMP-dependent code (that including \texttt{\#include <omp.h>} header) is to use the \texttt{-fopenmp} gcc's option.

This was by far, the easiest implementation to construct since it could even have been done without removing the OpenCV function calls, as the code runs in the processor's different threads the routines had been compiled for their architecture. This sums up to the fact that there is no need to conduct many changes in the sequential code to parallelize in the way we exposed in this chapter's Overview. The code for this version can be found in the \texttt{stabilization\_omp.cpp} source file.

By using OpenMP's pragma constructions, we annotate the code to declare which fractions of memory are shared and which ones are private, how to schedule the distribution of the thread blocks and also to tell the compiler that the iterations of the two \texttt{for} loops are the ones to be distributed among the threads set with \texttt{omp\_set\_num\_threads()} function.

\begin{itemize}
    \item \texttt{\#pragma omp parallel for} -- this \textit{pragma} is a shortcut to specify a parallel construct that forms a team of threads based on the number of iteration of the loop(s) and starts their parallel execution.
    \item \texttt{private(m\_pos, n\_pos, Sp, Res)} -- the local variables for the threads, meaning that each one would have a \textbf{different} copy of them. In order: the index to displace the search window on the X-axis, the equivalent for the Y-axis, the corresponding previous frame's search window and the result of applying the XOR operation.
    \item \texttt{shared(Sc, past, Cj, elements, p)} -- these would be the shared memory portions between all the streams, all will hold the same copy: the current frame's search window, the past frame's whole subimage, the correlation matrix and the size in pixels of the subimage to crop it. (Also \texttt{p} have to be included as it is determined at runtime).
    \item \texttt{schedule(static)} -- this clause tells the scheduler how to distribute the iterations to the threads, in our case, assigned in round-robin fashion. 
\end{itemize}

\subsection{CUDA} \label{sec:impl_cuda}

When it comes to compile CUDA code, the tools have evolved to nowadays' NVIDIA CUDA Compiler (\texttt{nvcc}) which eases the process that involves splitting, compiling, preprocessing, and merging intermediate files from CUDA kernels (\texttt{.cu}) to fully executable code. Still, to compile the code we have to worry about the GPU architecture details of the machine since different generations of cards offer different memory disposition and the capability to launch different number of threads, etc.

As we exposed in chapter \ref{ch:2}, the way to work with CUDA code comes in the form of kernels: plain-C functions that are run by the SMs in what they call \textit{thread blocks}, the key point when working with this paradigm is to correctly specify the geometry of this blocks since we are translating the loops that filled the correlation matrix in the sequential approach to a uniform distribution of their iterations between all the SMs available in the device. In our case, we choose blocks of 256 threads as a conservative approach to almost all CUDA architectures, and partitioning $C_{j}$ in $n/256$ where $n$ is its number of elements.

In our case, the file \texttt{kernel.cu} is the responsible of this task and contains almost the exact same code we had at the end of the loop. One important consideration to make is that to call the helper functions from both the host and the device code, as we do, is that we must annotate them with the labels \texttt{\_\_host\_\_} and \texttt{\_\_device\_\_} at the same time to tell the compiler to build them for both architectures.

Also, as it was described in the section \ref{subs:gpgpu} it is essential to correctly allocate and distribute the memory elements in both the host and the device. To do so and make our code more legible, we followed some community conventions as to label with the prefix \texttt{d\_} the variables that are allocated and used in the device and with an \texttt{h\_} their counterpart in the host.

These variables to copy in the device memory would be the same we pointed out as \texttt{shared} in the OpenMP version, but in this case, we have to specify the direction in which we would made the copy as we are talking about different address spaces. Obviously, the subimage and the search window need to be moved from host to the device and the opposite for the correlation matrix. All this operative is described in \texttt{stabilization\_cuda.cpp}.

We have to point out that as calling this operations inside a loop make them the bottleneck of the solution, since the allocations and copies are one of the most time-consuming steps. Considering the use of three dimensional thread-blocks along with synchronization mechanisms could remove this overhead and speed up noticeably the whole program.

\subsection{OpenCL} \label{sec:impl_opencl}

To build an OpenCL program, the procedures vary depending on the OpenCL implementation in use, as it is made for different environments. The absence of an proprietary compiler like the \texttt{nvcc}\footnote{There were some attempts made in this direction by the community, as it was the \texttt{clcc} (\url{http://clcc.sourceforge.net/}) but as the OpenCL standard advanced, the tools did not and stay outdated.} together with the heterogeneous ideal of running in that many different platforms makes the whole process of compiling the code really hard for a basic user. Some difference between the 4 mainstream implementations (say: NVIDIA, AMD, Apple and Intel) is the location of the SDK path, which needs to be passed as an argument to the compiler, and also there are differences in this process based on whether if we are talking about C or C++ code. To handle all this complexity, we must rely on a \texttt{Makefile}. As example, we took one of the ones used for the code samples in the OpenCL Programming Book\cite{tsuchiyama2010opencl}. Nevertheless, a common denominator for all vendors is the use of the \texttt{-lOpenCL} flag in the compilation process.

The main difference found with CUDA comes as a result of the framework's stake to approach heterogeneous support in front of the very specific ecosystem controlled by NVIDIA itself. Everything must be done in a very \textit{runtime-aware} way: it introduces the idea of context, which is just a runtime link to the device and platform, \textbf{the kernel has to be loaded and compiled at runtime} which is indeed a very obscure way to proceed, since too many intermediate steps have to be done to make the kernel available and launched, etc.

The whole summarized process described in detail in \texttt{stabilization\_ocl.cpp} involves getting all the platform IDs (drivers), connect to compatible compute devices by creating a device group and assign it a compute context that allows the data transference between host and device and then initiate a command queue to execute the kernel in the devices selected. From that point, the program resembles very much the one we presented in the last section.

As a positive point, the only changes between the OpenCL and CUDA kernels are just the different terms used to annotate the functions and the shared memory portions passed as arguments (that in the case of OpenCL are marked with the \texttt{\_\_global} identifier) and also the way to access the indexes to calculate the right search window. As final note, this kind of kernels are based on C99 which made us port the helper functions to a \texttt{.c} file compliant with this standard.

\begin{table}[h!]
\centering
\resizebox{\textwidth}{!}{\begin{tabular}{|c|c|c|}
\hline
Index           & \textbf{CUDA kernel}                            & \textbf{OpenCL kernel}          \\ \hline
\texttt{m\_pos} & \texttt{blockIdx.y * blockDim.y + threadIdx.y}  & \texttt{get\_global\_id(1) * get\_global\_size(1) + get\_local\_id(1)}     \\ \hline
\texttt{n\_pos} & \texttt{blockIdx.x * blockDim.x + threadIdx.x}  & \texttt{get\_global\_id(0) * get\_global\_size(0) + get\_local\_id(0)}     \\ \hline
\end{tabular}}
\caption{Differences in the methods to access the indexes inside the kernels. \label{tab:indexes}}
\end{table}

As we advanced earlier, OpenCV has the ability to be enabled when the hardware is available with OpenCL, this way some computer vision operations could take advantage of the acceleration of the underlying platform for some of the built-in routines. Extending where we could take advantage of the parallelism in out program to the wrapper operations, the main sequential block of code of the program. This falls out of the scope of this project but could be an interesting topic for a possible complete parallel solution.

\section{Conformance Testing} 

As mentioned before, this method has been around for more than 15 years now and it can be still considered relevant and sufficient for many consumer solutions. The overall performance of the GC-BPM as video stabilization method was evaluated on section IV: \textit{Simulation Results} of \cite{GC-BPM1999} and it was one the reasons to choose it as frame topic for this project, it presents a highly reliable process at a very low price, as it works with very lightweight data structures, bitwise operators and also, as it has been discussed on this chapter could be parallelized in a pretty straightforward manner. 

But as our solution is not totally complete, meaning it does not produce any video output but instead the indicators that determine the direction and strength in which the video is shaking, we should test the reliability of both the sequential and different parallel implementations. For this purpose we propose two easy reference tests over the same data sets to evaluate the numerical outputs.

\begin{enumerate}
    \item \textbf{Tests with controlled image sequences}: if we decouple the frame extraction module of the wrapper and inject in memory both stabilized and shaking simple image sequences, while manually calculating the global motion vector by displacing the images in the axis of the canvas we can check the coherence of the result, as we do to present the interpretation of results in appendix \ref{app:results}.

    \item \textbf{Comparisons between parallel and sequential results}: this is straightforward and can be carried out after the general, sequential algorithm has been tested with the first procedure to ensure the results of all the programs are exactly the same no matter how many processing elements are involved for the calculations.
\end{enumerate}

% TODO: compilation
\iffalse
    As different APIs means different ways of compiling the source code, we proposed and developed a specific \texttt{Makefile} to construct the different binaries for the algorithm based on the APIs selected for this project and abstracting platform dependent details. In the following sections, we discuss the specifics of these implementations, their dependences and the way to construct the programs from a standard UNIX shell. We would return at this issue in chapter \ref{ch:5} when comparing some aspects of the APIs from a user/developer perspective.
\fi