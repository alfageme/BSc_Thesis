\chapter{Results} \label{ch:5}

\section{Methodology}

One of the key points of this work is to offer some indicators relative to the performance of the different tools put into practice in building the parallel solutions. To achieve this, we have developed a slightly rigorous methodology to compare the contrasting results achieved by the algorithms.

To carry out with the experiments, we selected a video from Michael Messer (\url{https://vine.co/v/iVWbAjB1XmH}), a famous \textit{viner}. Since the format of this vine videos is short enough for our purpose and usually shaky as they are recorded with smartphones. Also they have very convenient dimensions and frame rate for our experiments: Duration: 6s, 196 frames -- Resolution: 480x480 pixels.

Different runs will be made with different number of processing units for the purpose of comparing the speedup introduced as these numbers increase. To avoid undesirable variability around the measurements and to refine the results we will launch the experiments 3 times and calculate the average of their run times. Also, we have to make some notes on the number of executions that will be made with each tool regarding the number of processing elements:

\begin{itemize}
    \item Sequential: as we don't need to compare the different run times of the sequential version as we can't introduce more processing units by definition, we would only count with 3 runs with this configuration.
    \item OpenMP: these could be compared setting different threads, starting with the sequential equivalent (just one) and doubling this number up to the maximum available for the particular machine (determined at runtime by the function \texttt{omp\_get\_max\_threads()}).
    \item CUDA: for this framework, we must determine the size of blocks, normal equivalence classes for the most common microarchitecture (NVIDIA Kepler) are 128, 256, 512 and 1024 threads. But some others have different inbuilt limits for execution.
    \item OpenCL: in this case, we execution model is mapped to the hardware, so the the work group size can be at most \texttt{DEVICE\_MAX\_WORK\_GROUP\_SIZE} and depending on the platforms and devices available at runtime, can be executed in a CPU or in a GPU, having really different possibilities.
\end{itemize}

\section{Experiments} \label{sec:experiments}

There is really important to correctly characterize the environment in which all the runs are going to be made, in two different dimmensions: the CPU and the GPUs available and their microarchitecture, since it determines 

The experiments by themselves can be performed on virtually any modern general purpose computer with the exception of those binaries that require of the CUDA platform to be run, which as mentioned in chapter \ref{ch:1} need some special hardware appliances (\url{https://developer.nvidia.com/cuda-gpus}) to be run. Both OpenMP and OpenCL implementations are open source and freely available for major manufacturer architectures. Because of this reason, \textbf{we have not been able to run the CUDA's implementation}, since to the date, we lack of a machine with these capabilities. However, the compilation phase was carried for all the implementations since \texttt{nvcc} is not related to the presence of a device.

\textbf{The speedup comparison between the tree implementations stays on hold until some system available to run all four is available for this purpose.} But nevertheless, we build all the tools and scripts to make this straightforward once the machine is ready. It will only take to compile and execute a couple of scripts to get the final results and graphics determining which one of the developed is the faster solution. We hope to have this results available for the defense of this project. 

%The particular hardware set-up where the data presented in this project was captured had the following underlying specs:

% TODO: replace with VM provided data from the FIT

\iffalse
\begin{itemize}
    \item Processor:
    \item GPU:
\end{itemize}
\fi

\section{Results}

To make the process of collecting the results of all the experiments simpler, we built a short script (\texttt{timecollector.py}) to dump the time indicators from the different program runs into a single \texttt{.csv} document to easily import those into an analysis tool, such as MS Excel or R lang, or just feed them to a \texttt{gnuplot} script to generate simple but meaningful graphics. This temporal data is collected by the script into a file with the following structure: 

\VerbatimInput[frame=single]{misc/data.csv}

It contains four differenced columns separated by commas:

\begin{itemize}
    \item \texttt{Run}: The sequence number of the run, just to discriminate and later refer to a specific experiment.
    \item \texttt{API}: A numerical code associated to one of the APIs under study as it appears in \texttt{commons.h}: \texttt{1} for the sequential version of the algorithm, \texttt{2} for OpenMP, \texttt{3} for CUDA and \texttt{4} for the OpenCL version.
    \item \texttt{Processing\_Units}: the number of streams (threads, threads per block or the size of the workgroup) involved in that particular run.
    \item \texttt{Time}: the really relevant variable in each row; reflects how long did it take for the program to determine all the global motion vectors.
\end{itemize}

\subsection{Comparison of the APIs}

We already offered the the really important points to compare these APIs from a developer's practical point of view thought sections \ref{sec:impl_openmp}, \ref{sec:impl_cuda} and \ref{sec:impl_opencl}. Where the main differences in matter of implementation where exposed. In this section we will add some notes to this comparative about the user experience.

The main problem when trying to measure aspects like the ease of use and user friendlyness is that those are both very relative aspects varying among developers. But we can still propose some opinionated metrics and show their values relative to this particular project:

\begin{enumerate}
    \item Extra lines of code of the port as taken from the reference sequential algorithm.
    \item Amount of official documentation and relative quality provided by the manufacturer/consortium behind the API.
    \item Approximate total time to make the solution compile.
    \item Number of failed versions before getting the right results.
\end{enumerate}

\begin{table}[h!]
    \centering
    \begin{tabular}{|c|c|c|c|}
    \hline
                & \textbf{OpenMP}           & \textbf{CUDA}                                               & \textbf{OpenCL}              \\ \hline
    \textit{1.} 
                & 6                         & 21 + 16 (kernel) = 37                                       & 37 + 16 (kernel) = 53        \\ \hline
    \textit{2.}\tablefootnote{Some details and examples will be given later in this text.} 
                & Poor and unstructured     & Complete and plenty                                         & Limited and lacking examples \\ \hline
    \textit{3.} 
                & 2h                        & 10h                                                         & 11h                          \\ \hline
    \textit{4.} 
                & 1                         & \textbf{?}\tablefootnote{See section \ref{sec:experiments}} & 12                           \\ \hline
    \end{tabular}
    \caption{Comparison of the different metrics considered for the 3 frameworks. \label{tab:metrics}}
\end{table}

One aspect that is necessary to mention is the hard that has been to make all the APIs correctly work with a relatively modern setups, the exception would be OpenMP which is standardized in a really solid way that make it available in almost every environment and compiler. Special mention in this case goes to OpenCL, which have been particularly difficult to build and compile. Offering which we can call a \textit{very obscure API}. We can get a glimpse of this just by taking a look into AMD's official introductory tutorial to OpenCL \cite{amdOpenCLTutorial}, to see the humongous number of steps we have to take to make a simple Hello World style program with the framework.

Regarding the documentation and material offered by the responsible of maintaining the different APIs, we have some notes to make:

\begin{itemize}
    \item OpenMP ARB does not offer any de facto documentation, but rather links \cite{OMPdocs} to community tutorials, books and examples.
    \item The NVIDIA developer zone \cite{nvidia} is a really great compilation of resources for the CUDA platform. It contains, from installation instructions, to the whole API reference and some guides to speed up the implementations, learn how to use their tools, and many more. Is the most updated (Sep. 2015) of the three analyzed here.
    \item Khronos Group, the association behind OpenCL standard, has a nice structured but really limited and outdated (2010) documentation \cite{OCLdocs}, that also lacks of examples like the ones found in CUDA's and is constantly linking the OpenCL Specification, a 400 pages very technical document that does constitute a really good documentation repository.
\end{itemize}

\subsection{General Comparison Between CUDA and OpenCL} \label{sec:compCUDA-CL}

Additionally, as CUDA and OpenCL frameworks rely on the kernel execution model, they are the most sensitive to be compared. In this section, we will mention some differences and facts with relevance when deciding between these two.

While OpenCL can natively talk to a large range of devices from different vendors, that doesn't imply that the code will run optimally on all of them without overhead and also there is no guarantee it will even run, given that different devices have very different instruction sets. If we stick to the OpenCL spec and avoid vendor-specific extensions, the code should be portable, if not tuned for speed. For now the programmer must face some efforts on the scale of a rewrite of the whole kernel code when switching devices for nontrivial programs. Fortunately though, the host-side stays the same across devices. 

OpenCL does not support pinned host memory. This may cause a penalty of about a factor of two in host-device transfer rates. Pinned memory is memory allocated using functions like \texttt{cudaMallocHost}, which prevents the memory from being swapped out and provides better transfer speeds. Non-pinned memory is memory allocated using common \texttt{malloc} function. In this sense, pinned memory is much more expensive to allocate and deallocate but provides higher transfer throughput for large memory transfers. \cite{CUDAvsOpenCL}

CUDA's synchronization features are not as flexible as those of OpenCL where any queued operation (memory transfer, kernel execution) can be told to wait for any other set of queued operations. CUDA's streams are more limited then, summed up to the fact that OpenCL supports synchronization across multiple devices. CUDA has more mature tools, including its own compiler, debugger and profiler while OpenCL does not have this kind of support. \cite{CUDAvsOpenCL}

As we mentioned through the first chapters of this work, the world of High Performance Computing as it exists today consists of many GPU-accelerated systems and applications. As it was the pioneer in the field, NVIDIA's CUDA in some ways concentrated the monopoly as both programming language and infrastructure. The problem with this model is that software is almost always strictly controlled and its source code is most often propietary, and the hardware options are limited to one vendor.

Recently, some efforts have been made in order to make CUDA a platform more portable. In this sense, in 2015 major NVIDIA competitor, AMD presented the ``Boltzmann Initiative'' \cite{amdBoltzmann} that included something called Heterogeneous-compute Interface for Portability or HIP offering several benefits over other heterogeneous programming interfaces, like OpenCL:

\begin{itemize}
    \item Developers can code in C++, and mix host and device C++ code in their source files.
    \item HIP API is less verbose than OpenCL.
    \item Because both CUDA and HIP are C++ languages, porting from CUDA to HIP is significantly easier than porting from CUDA to OpenCL.
    \item HIP offers an offline compilation model where the kernel binary is read in by the host code.
\end{itemize}