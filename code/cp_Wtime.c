inline double cp_Wtime(){
	struct timeval tv;
	gettimeofday(&tv, (void *) 0);
	return tv.tv_sec + 1.0e-6 * tv.tv_usec;
}