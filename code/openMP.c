double timeStart, timeEnd;
timeStart = omp_get_wtime();
	// Portion of the program to be measured
timeEnd = omp_get_wtime();

printf("Total time spent: %f\n", timeEnd - timeStart);