// initialize the two events
cudaEventCreate(&startT);
cudaEventCreate(&endT);

// enclose kernel launch between startT and endT events
cudaEventRecord(startT, str);
doSomething<<<grid, block, 0, str>>>(...); // launch request has to be placed in a stream.
cudaEventRecord(endT, str);

// wait for endT event to take place
cudaEventSynchronize(endT);

// calculate elapsed time
cudaEventElapsedTime(&duration, startT, endT);
printf("Kernel executed for %f\n", duration);