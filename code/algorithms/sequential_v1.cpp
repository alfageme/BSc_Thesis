#include <opencv2/opencv.hpp>

#include "commons.h"
#include "stabilization.h"

/* Gray Coded Approach based on:
 * '\cite{GC-BPM1999}' S. Ko, S. Lee, S. Jeon, and E. Kang. Fast digital image 
 *      stabilizer based on gray-coded bit-plane matching.
 * ------------------------------------------------------------------
 */
void GC_BPM(cv::Mat Scurr, cv::Mat Sprev, int Vg_prev_x, int Vg_prev_y
        , int *Vg_x, int *Vg_y)
{      
     int rows(Scurr.rows), cols(Scurr.cols);

     // Proportions of the search window and margins
     int N = 120;
     int p = (rows/2 - N)/2;

     // Size of the search window: squared (A. Brooks '\cite{Brooks2003}' approach)
     int SW_SIZE = N * N;

     // Local motion vectors (0-3)
     int Vl_x[5];
     int Vl_y[5];

     // We take into account the previous global motion vector (4)
     Vl_x[4] = Vg_prev_x;
     Vl_y[4] = Vg_prev_y;
     
     // Break both previous and current frames into 4 subimages:
     std::vector<cv::Mat> S(4);
     std::vector<cv::Mat> S_prev(4);

     S_prev.at(0) = Sprev(cv::Rect(0,0,cols/2,rows/2));
     S_prev.at(1) = Sprev(cv::Rect(cols/2,0,cols/2,rows/2));
     S_prev.at(2) = Sprev(cv::Rect(0,rows/2,cols/2,rows/2));
     S_prev.at(3) = Sprev(cv::Rect(cols/2,rows/2,cols/2,rows/2));
     
     S.at(0)      = Scurr(cv::Rect(0,0,cols/2,rows/2));
     S.at(1)      = Scurr(cv::Rect(cols/2,0,cols/2,rows/2));
     S.at(2)      = Scurr(cv::Rect(0,rows/2,cols/2,rows/2));
     S.at(3)      = Scurr(cv::Rect(cols/2,rows/2,cols/2,rows/2));

     int m_pos, n_pos;

     // Iterate over the 4 different search windows
     for(int i=0 ; i<4 ; i++){
        // Correlation matrix to store the results of the search
        int Cj[2*p][2*p];

         // Displacement loops: exhaustive search
        for(m_pos=0 ; m_pos < 2*p ; m_pos++){
            for(n_pos=0 ; n_pos < 2*p ; n_pos++){
                cv::Mat bxor;

                cv::bitwise_xor(
                    // Current bit-plane search window:
                    S.at(i)(cv::Rect(p, p, N, N)), 
                    // Previous bit-plane search window:             
                    S_prev.at(i)(cv::Rect(m_pos, n_pos, N, N)), 
                    // Result of the XOR between the 2:
                    bxor                                        
                );

                // Correct OpenCV XOR operation (see section '\ref{subs:problems}')
                cv::bitwise_not(bxor,bxor);

                // Count the black pixels as (Total no. - Non-Black):
                Cj[m_pos][n_pos] = SW_SIZE - cv::countNonZero(bxor);
            }
        }
        // Determine position of the min Cj arguments
        argMin(2*p, (int *)Cj, &Vl_x[i], &Vl_y[i]);
     }
     *Vg_x = median(Vl_x);
     *Vg_y = median(Vl_y);
}