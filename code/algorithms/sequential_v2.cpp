#include <opencv2/opencv.hpp>

#include "commons.h"
#include "stabilization.h"

/* Gray Coded Approach based on:
 * [14] S. Ko, S. Lee, S. Jeon, and E. Kang. Fast digital image stabilizer based
 * on gray-coded bit-plane matching. IEEE Transactions on Consumer Electronics,
 * vol. 45, no. 3, pp. 598-603, Aug. 1999.
 * ----------------------------------------------------------------------------
 */
void GC_BPM(cv::Mat Scurr, cv::Mat Sprev, int Vg_prev_x, int Vg_prev_y, 
    int *Vg_x, int *Vg_y)
{      
     int rows(Scurr.rows), cols(Scurr.cols);

     // TODO: badly formulated - rewrite to make N related to video proportions
     int N = 120;
     int p = (rows/2 - N)/2;

     // Size of the search window, assuming it squared (A. Brooks [6] approach)
     int SW_SIZE = N * N;

     // Local motion vectors (0-3)
     int Vl_x[5];
     int Vl_y[5];

     // We take into account the previous global motion vector (4)
     Vl_x[4] = Vg_prev_x;
     Vl_y[4] = Vg_prev_y;
     
     // Break both previous and current frames into 4 subimages:
     std::vector<cv::Mat> S(4);
     std::vector<cv::Mat> S_prev(4);

     S_prev.at(0) = Sprev(cv::Rect(0,0,cols/2,rows/2)).clone();
     S_prev.at(1) = Sprev(cv::Rect(cols/2,0,cols/2,rows/2)).clone();
     S_prev.at(2) = Sprev(cv::Rect(0,rows/2,cols/2,rows/2)).clone();
     S_prev.at(3) = Sprev(cv::Rect(cols/2,rows/2,cols/2,rows/2)).clone();
     
     S.at(0)      = Scurr(cv::Rect(0,0,cols/2,rows/2)).clone();
     S.at(1)      = Scurr(cv::Rect(cols/2,0,cols/2,rows/2)).clone();
     S.at(2)      = Scurr(cv::Rect(0,rows/2,cols/2,rows/2)).clone();
     S.at(3)      = Scurr(cv::Rect(cols/2,rows/2,cols/2,rows/2)).clone();

     int dimension = (SW_SIZE + 8 - 1) / 8;

     unsigned char* Sc  = new unsigned char[dimension]();
     unsigned char* Sp  = new unsigned char[dimension]();
     unsigned char* Res = new unsigned char[dimension]();

     int m_pos, n_pos;

     for(int i=0 ; i<4 ; i++){
        int Cj[2*p][2*p];

        int elements = S.at(i).cols;
        
        unsigned char* current = S.at(i).data;
        unsigned char* past    = S_prev.at(i).data;

        unsigned char* c2 = new unsigned char[SW_SIZE]();

        splitMat(current, elements, p, p, N, c2);
        mat2arr(c2, SW_SIZE, Sc);

        for(m_pos=0 ; m_pos < 2*p ; m_pos++){
            for(n_pos=0 ; n_pos < 2*p ; n_pos++){

                unsigned char* p2 = new unsigned char[SW_SIZE]();
                splitMat(past, elements, m_pos, n_pos, N, p2);

                mat2arr(p2, SW_SIZE, Sp);
                xor2img(Sc, Sp, Res, dimension);
                Cj[m_pos][n_pos] = count1s(Res,dimension);
            }
        }
        argMin(2*p, (int *)Cj, &Vl_x[i], &Vl_y[i]);
    }

    delete[] Sc;
    delete[] Sp;
    delete[] Res;

    *Vg_x = median(Vl_x);
    *Vg_y = median(Vl_y);
}