\chapter{CD Contents}

\begin{itemize}
    \item Source code for the solutions presented in chapter \ref{ch:4} in the \texttt{src} directory.
    \begin{itemize}
        \item \texttt{v1} -- Sequential version using OpenCV presented in \ref{sec:solution}.
        \item \texttt{v2} -- \textbf{The final sequential solution and their parallel equivalents.}
        \item \texttt{test} -- Simple source code and image examples used to test the helper functions in appendix \ref{app:tests}.
    \end{itemize}
    \item \LaTeX \ source code of this document in \texttt{latex} directory.
    \item The video file used as example to be stabilized, named \texttt{video.mp4}.
    \item The tools to get the speedup results: \texttt{timecolector.py} and \texttt{gnuplot} scripts to elaborate the graphics. In the \texttt{tools} folder.
    \item This document compiled in PDF format: \texttt{BP\_FIT\_Samuel\_Alfageme\_2016\_v2.pdf}.
\end{itemize}

\chapter{Sequential Implementation} \label{app:implementations}

\section{First Approach -- \texttt{v.1.0}: Use of OpenCV}

\lstinputlisting[style=custom, escapechar=']{code/algorithms/sequential_v1.cpp}

\section{Second Approach -- \texttt{v.2.0}: Independent Algorithm}

As all the preprocessing work from \texttt{v.1.0} is the same for this second version, we only present here the core differences.

\lstinputlisting[style=custom, firstnumber=54, firstline=54, lastline=79]{code/algorithms/sequential_v2.cpp}

\chapter{Conformance Tests carried on the Helper Functions} \label{app:tests}

As for version 2 we no longer relied on functions of some trustworthy framework standard library, we were in need of performing some simple test to check the correctness of the helper methods developed.

For the sake of simplicity, we created the following 2 10x10px images: \texttt{img1.png} and \texttt{img2.png} to see if the process of packing the images into \texttt{unsigned char} (or bytes) data structures was being carried out properly. 

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.2\textwidth}
        \includegraphics[width=\textwidth]{misc/img1.png}
        \caption{Enlargement of \texttt{img1.png}}
    \end{subfigure}
    \quad
    \begin{subfigure}[b]{0.2\textwidth}
        \includegraphics[width=\textwidth]{misc/img2.png}
        \caption{Enlargement of \texttt{img2.png}}
    \end{subfigure}
\end{figure}

\begin{multicols}{2}
    \VerbatimInput{misc/img1.txt}
    \VerbatimInput{misc/img2.txt}
\end{multicols}

In the previous listing, we can see, for both of the images, in the left an ASCII representation of each 10x10 matrix followed by the result in memory after executing the function \texttt{mat2arr()} in them as observed in a debugger program. As the reader can see, all the data is correctly packed and coalescent as it was explained in chapter \ref{ch:5}.

The ``core'' function of the algorithm, the one that performs the exclusive-or operation between two images, was also tested to see if the operative was sound. To do so, we invoked the operation between the two presented matrices, which resulted in the following memory allocation and, when unpacked according to the original matrix dimensions, can be translated in the ASCII equivalent of the matrix presented by its side. We created the \texttt{PNG} version using OpenCV's functions to check their match.

\begin{figure}[h!]
    \centering
	\includegraphics[width=0.3\textwidth]{misc/xor.png}
	\caption{Enlargement of \texttt{xor.png}}
\end{figure}

\VerbatimInput{misc/xor2img.txt}

To test the \texttt{count1s()} function, we calculated the number of black pixels for each one of the previous matrices, passing the arrays in which they were packed, resulting in 55 for \texttt{img1.png}, 64 for \texttt{img2.png} and 49 for the resulting \texttt{xor.png}, the same numbers that we can count in the presented images.

Finally, the \texttt{splitMat()} function was used with the arguments \texttt{(img1, 10, 3, 3, 4, result)} to simulate the selection of a centered 4x4 search window over \texttt{img1.png} as can be seen in figure \ref{fig:submat}. This printed out the following results that correspond with the 16 ordered pixels of the submatrix: 0 for the black and 255 for the white:

\VerbatimInput{misc/splitMat.txt}

\begin{figure}[t!]
    \centering
	\includegraphics[width=0.5\textwidth]{misc/img1_split.pdf}
	\caption{Area of the matrix selected to be split \label{fig:submat}}
\end{figure}

\chapter{Example Results} \label{app:results}

The output of executing any of the programs is a generated \texttt{results.txt} file containing the sequence of Global Motion Vectors ordered by frame.

\VerbatimInput[frame=single]{misc/results.txt}

To both describe how to interpret this results and give some instructions on how to complete the computer vision application to generate the stabilized video we injected two close frames in the algorithm (just to be more significant, they don't have to be consecutive) to obtain their GMV. If we trace a ``stabilization window'' with the right size by leaving a margin of $p$ in every side, the meaning of the vector is the displacement of this window from the reference frame to the other. For the particular case of the two frames selected on the figure \ref{fig:window} this GMV was $(-5,-9)$ and the resulting stabilized video will be the consecutive cropped areas in the images.

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.35\textwidth}
        \includegraphics[width=\textwidth]{fig/discarded.pdf}
        \caption{Reference frame ($n$)}
    \end{subfigure}
    \quad
    \begin{subfigure}[b]{0.35\textwidth}
        \includegraphics[width=\textwidth]{fig/discarded2.pdf}
        \caption{Compared frame ($n+4$)}
    \end{subfigure}
    \caption{Displacement of the stabilized window between the two frames \label{fig:window}}
\end{figure}



